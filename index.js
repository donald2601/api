const express = require('express')
const app = express()
 const port = 3001
 import mongoose from 'mongoose'
 import bodyParser from 'body-parser'
 import routes from './src/routes/routes'
 import cors from 'cors'
 app.use(cors());

 //CONNEXION MONGOOSE
 mongoose.connect('mongodb://localhost/BLOG_2',{useNewUrlParser:true})
.then(()=> console.log('la connexion est un succes'))
.catch((err)=> console.error(err))

//CONFIGURATION DE BODY-PARSER
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

//ROUTES
routes(app);

 
 app.listen(port, ()=>{
    console.log("tout es pret");
 })