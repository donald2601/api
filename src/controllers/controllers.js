import blogSchema from '../models/model'

export const addBlog = (req,res)=>{
    let newBlog = new blogSchema(req.body);
    newBlog.save((err,blog)=>{
        if (err) {
            res.send(err.message)
        }
        res.json(blog)
    })
}

export const getBlog = (req,res)=>{
    blogSchema.find({},(err,blog)=>{
        if (err) {
            res.send(err.message)
        }
        res.json(blog)
    })
}

export const getBlogById = (req,res)=>{
    blogSchema.findById((req.params.blogId),(err,blog)=>{
        if (err) {
            res.send(err.message)
        }
        res.json(blog)
    })
}

export const putBlog = (req,res)=>{
    blogSchema.findOneAndUpdate({_id : req.params.blogId},req.body,{new:true},(err,blog)=>{
        if (err) {
            res.send(err.message)
        }
        res.json(blog)
    })
}

export const deleteBlog = (req,res)=>{
    console.log(req.params);

    blogSchema.findByIdAndDelete((req.params.blogId),(err,blog)=>{

        if (err) {
            res.send(err.message)
        }
        res.status(200).json({message: "delete avec success"})
        console.log(req.params);
    })
}