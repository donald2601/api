import { addBlog, getBlog, getBlogById, putBlog, deleteBlog } from "../controllers/controllers";

const routes = (app)=>{
    app.route('/')
    .get(getBlog);
    
    app.route('/blog')
        .get(getBlog)

        .post(addBlog);

    app.route('/blog/:blogId')
        .get(getBlogById)

        .put(putBlog)

        .delete(deleteBlog);
}

export default routes;
